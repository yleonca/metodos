import java.util.Scanner;

/**
 *
 * @author DESARROLLADOR
 */
public class Ejemplo {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String nombre;
        double edad1;
        char edad2;
        int edad3;

        System.out.print("Introduzca 3 valores de edad:");
        edad3 = sc.nextInt();
        edad2 = sc.next().charAt(0);
        edad1 = sc.nextDouble();

        System.out.println("Dato Tipo Entero" + edad3);
        System.out.println("Dato Tipo char" + edad2);
        System.out.println("Dato Tipo Double" + edad1);
        sc.nextLine();
        System.out.print("Introduzca su nombre:");
        nombre = sc.nextLine();
        System.out.println("Hola  " + nombre + "!!");

    }
}
